import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
 

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
   API: string = 'http://localhost:3000/products';
   data: any;
  constructor(private http: HttpClient) { }


  ngOnInit(): void {
    this.http.get(this.API).subscribe(
      res => {  
        this.data = res;
      }, err =>{
        alert(err);
      }
    );
  }

}
