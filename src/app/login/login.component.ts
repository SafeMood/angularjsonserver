import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = null;
  errorMessage: string;
  isLoggedIn: boolean;
  constructor(private authSerivce: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  tryLogin(data) {
    this.isLoggedIn = this.authSerivce.doLogin(data)
    if (this.isLoggedIn) {
      this.router.navigate(['/home']);
    } else {
      this.errorMessage = "Email or password are wrong"
    }
  }

}
