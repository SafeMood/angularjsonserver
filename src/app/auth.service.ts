import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  API: string = "http://localhost:3000/users";
  users :any;



  constructor(private http: HttpClient) {
    this.getUsers().subscribe(data => {
      this.users = data
    })
  }

  doLogin(data): boolean {
    return this.users.find(item => {
      if (data.email == item.email && data.password == item.password) {
        console.log("true");
        return true;
      }
      console.log("false");
      return false;
    });
  }

  getUsers() {
    return this.http.get(this.API)
  }
}


